package org.porgrad.tw;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.InputMismatchException;

public class LastSunday {

    public ArrayList<Integer> getLastSunday(int year) {
        if (year < 1 || year > 9999) {
            throw new InputMismatchException();
        }

        ArrayList<Integer> sunday = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            sunday.add(lastSundayOfAMonth(i, year));
        }
        return sunday;
    }

    private int lastSundayOfAMonth(int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, 1);
        calendar.add(Calendar.DATE, -1);
        calendar.add(Calendar.DAY_OF_MONTH, -(calendar.get(Calendar.DAY_OF_WEEK) - 1));
        return calendar.get(Calendar.DATE);
    }

}
package org.porgrad.tw;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LastSundayTest {

    LastSunday lastSunday = new LastSunday();

    @Test
    void shouldReturnTheLastSundayOfEveryMonthInTheGivenYear() {

        ArrayList<Integer> actualResult = lastSunday.getLastSunday(2022);
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(30, 27, 27, 24, 29, 26, 31, 28, 25, 30, 27, 25));

        assertThat(actualResult, is(equalTo(expectedResult)));
    }

    @Test
    void shouldThrowInputMismatchException() {
        assertThrows(InputMismatchException.class, () -> lastSunday.getLastSunday(10000));
    }
}